var React = require('react');

var ListItems = React.createClass({
    render: function() {
        return (
          <li className="list-group-item">
            <h4>{ this.props.text }</h4>
          </li>
        );
    }
});

// Export Module

module.exports = ListItems;
